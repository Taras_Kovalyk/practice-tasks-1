using System;
using System.Collections.Generic;
using System.Linq;

namespace PracticeTasks.Task1
{
    /// <summary>
    /// Class contains one double value and
    /// overloaded operators (+, *, /);
    /// </summary>
    public class Minor
    {
        #region Constructors
        public Minor() { }

        public Minor(double item)
        {
            this.Value = item;
        }
        #endregion

        #region Properties
        public double Value { get; private set; }
        #endregion

        #region Overloaded operators
        public static Minor operator +(Minor me, Minor other)
        {
            return new Minor(me.Value + other.Value);
        }

        public static Minor operator *(Minor me, Minor other)
        {
            return new Minor(me.Value * other.Value);
        }

        public static Minor operator /(Minor me, Minor other)
        {
            return new Minor(me.Value / other.Value);
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return this.Value.ToString();
        }
        #endregion
    }

    /// <summary>
    /// Class contains matrix (m x n) with doubles and 
    /// several methods for calculating minors of all kinds. 
    /// </summary>
    public class MatrixMinors
    {
        #region Constructors
        public MatrixMinors()
        {
            this.Matrix = new double[0, 0];
        }

        /// <summary>
        /// Creates matrix of specified size filled with zeroes.
        /// </summary>
        public MatrixMinors(int height, int width)
        {
            this.Matrix = new double[height, width];
        }

        /// <summary>
        /// This contructor allows to instantiate matrix with 
        /// several vectors, just as standart c# multi-dimensional array.
        /// </summary>
        public MatrixMinors(params IEnumerable<double>[] vectors)
        {
            if (vectors.Any(item => item.Count() != vectors.FirstOrDefault().Count()))
            {
                throw new ArgumentException("Vectors have different sizes");
            }

            this.Matrix = new double[vectors.Count(), vectors.FirstOrDefault().Count()];

            for (var i = 0; i < vectors.Count(); ++i)
            {
                for (var j = 0; j < vectors[i].Count(); ++j)
                {
                    this.Matrix[i, j] = vectors[i].ToArray()[j];
                }
            }
        }
        #endregion

        #region Properties
        public double[,] Matrix { get; private set; }

        public int Height
        {
            get
            {
                return this.Matrix.GetLength(0);
            }
        }

        public int Width
        {
            get
            {
                return this.Matrix.GetLength(1);
            }
        }
        #endregion

        #region Methods
        #region Public methods
        /// <summary>
        /// Minor determinant of specified order for any matrix.
        /// </summary>
        /// <param name="order">Order should be less than matrix size. Otherwise causes exception.</param>
        public Minor GetMinor(int order)
        {
            ValidateIfOrderIsLessThanSize(this.Matrix, order);

            return new Minor(GetDeterminant(this.Matrix, order));
        }

        /// <summary>
        /// (row,column) minor determinant for equilateral matrix.
        /// (InvalidOperationException for non-equilateral matrix)
        /// </summary>
        public Minor GetMinor(int row, int column)
        {
            ValidateIfOrderIsLessThanSize(this.Matrix, row);
            ValidateIfOrderIsLessThanSize(this.Matrix, column);
            ValidateIfEquilateral(this.Matrix);

            var submatrix = GetSubmatrix(this.Matrix, row, column);

            return new Minor(GetDeterminant(submatrix, Height - 1));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order">Order should be less than matrix size. Otherwise causes exception.</param>
        /// <returns></returns>
        public Minor GetComplementaryMinor(int order)
        {
            ValidateIfEquilateral(this.Matrix);
            ValidateIfOrderIsLessThanSize(this.Matrix, order);

            var submatrix = GetSubmatrix(this.Matrix, order);

            return new Minor(GetDeterminant(submatrix, Height - order));
        }
        #endregion
        #region Private methods
        private static double GetDeterminant(double[,] matrix, int current)
        {
            double det = 0;
            double[,] submatrix = new double[current - 1, current - 1];

            if (current == 2)
            {
                det = matrix[0, 0] * matrix[1, 1] - matrix[1, 0] * matrix[0, 1];
            }
            else
            {
                for (var k = 0; k < current; ++k)
                {
                    for (var i = 1; i < current; ++i)
                    {
                        var h = 0;
                        for (var j = 0; j < current; ++j)
                        {
                            if (j == k)
                                continue;
                            submatrix[i - 1, h] = matrix[i, j];
                            h++;
                        }
                    }
                    det += (Math.Pow(-1.0, k) * matrix[0, k] * GetDeterminant(submatrix, current - 1));
                }
            }

            return det;
        }

        private double[,] GetSubmatrix(double[,] matrix, int order)
        {
            var submatrix = new double[matrix.GetLength(0) - order, matrix.GetLength(0) - order];

            for (var i = 0; i < matrix.GetLength(0) - order; ++i)
            {
                for (var j = 0; j < matrix.GetLength(0) - order; ++j)
                {
                    submatrix[i, j] = matrix[i + order, j + order];
                }
            }

            return submatrix;
        }

        private double[,] GetSubmatrix(double[,] matrix, int row, int column)
        {
            var submatrix = new double[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];

            int rowIndex = 0;

            for (var i = 0; i < matrix.GetLength(0); ++i)
            {
                var columnIndex = 0;

                if (i == row)
                {
                    continue;
                }

                for (var j = 0; j < matrix.GetLength(0); ++j)
                {
                    if (j == column)
                    {
                        continue;
                    }

                    submatrix[rowIndex, columnIndex] = matrix[i, j];
                    columnIndex++;
                }
                rowIndex++;
            }

            return submatrix;
        }

        private static void ValidateIfEquilateral(double[,] matrix)
        {
            if (matrix.GetLength(0) != matrix.GetLength(1))
            {
                throw new InvalidOperationException("Matrix is not equilateral.");
            }
        }

        private static void ValidateIfOrderIsLessThanSize(double[,] matrix, int order)
        {
            if (order > Math.Min(matrix.GetLength(0), matrix.GetLength(1)))
            {
                throw new ArgumentException("Order is larger than matrix size.");
            }
        }
        #endregion
        #endregion
    }
}